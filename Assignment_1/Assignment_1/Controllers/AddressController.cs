﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.Controllers
{
    public class AddressController : Controller
    {
        Models.ManagerEntities5 context = new Models.ManagerEntities5();
        // GET: Address
        public ActionResult Index(int id)
        {
                var theProfile = context.ProfileInformations.Single(x => x.profile_id == id);

                return View(theProfile);
            
        }

        // GET: Address/Details/5
        public ActionResult Details(int id)
        {
            var theAddress = context.AddressInformations.Single(x => x.address_id == id);

            ViewBag.id = theAddress.profile_id;

            return View(theAddress);
        }

        // GET: Address/Create
        public ActionResult Create(int id)
        {
            
            ViewBag.id = id;
            ViewBag.countries = new SelectList(context.Countries, "Id", "country_name");
            return View();
        }

        // POST: Address/Create
        [HttpPost]
        public ActionResult Create(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                Models.AddressInformation newAddress = new Models.AddressInformation()
                {
                    description = collection["description"],
                    street_address = collection["street_address"],
                    city = collection["city"],
                    province = collection["province"],
                    zip_code = collection["zip_code"],
                    profile_id = id,
                    country_id = int.Parse(collection["country_id"])
                  
                };

                TryValidateModel(newAddress);

                if (ModelState.IsValid)
                {

                    context.AddressInformations.Add(newAddress);
                    context.SaveChanges();
                    return RedirectToAction("Index", new { id = id });
                }
                return View(newAddress);
            }
            catch
            {
                return View();
            }
        }

        // GET: Address/Edit/5
        public ActionResult Edit(int id)
        {
            var theAddress = context.AddressInformations.Single(x => x.address_id == id);
            ViewBag.id = theAddress.profile_id;
            ViewBag.countries = new SelectList(context.Countries, "Id", "country_name");
            return View(theAddress);
        }

        // POST: Address/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                Models.AddressInformation newAddress = context.AddressInformations.Single(x => x.address_id == id);

                newAddress.description = collection["description"];
                newAddress.street_address = collection["street_address"];
                newAddress.city = collection["city"];
                newAddress.province = collection["province"];
                newAddress.zip_code = collection["zip_code"];
                newAddress.country_id = int.Parse(collection["country_id"]);
             TryValidateModel(newAddress);

            if (ModelState.IsValid)
            {
                context.SaveChanges();

                var redirect = newAddress.profile_id;

                return RedirectToAction("Index", new { id = redirect });
            }
            return View(newAddress);
              }
              catch
              {
               return View();
              }
        }

        // GET: Address/Delete/5
        public ActionResult Delete(int id)
        {
            var theAddress = (from a in context.AddressInformations
                              where a.address_id == id
                              select a).FirstOrDefault();
            ViewBag.id = theAddress.profile_id;
            return View(theAddress);
        }

        // POST: Address/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                var theAddress = context.AddressInformations.Single(x => x.address_id == id);

                var redirect = theAddress.profile_id;
                context.AddressInformations.Remove(theAddress);
                context.SaveChanges();
               
                return RedirectToAction("Index",  new { id = redirect });
            }
            catch
            {
                return View();
            }
        }
    }
}
