﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment_1.ActionFilters;

namespace Assignment_1.Controllers
{
    public class ChatController : Controller
    {
        Models.ManagerEntities5 context = new Models.ManagerEntities5();

        // GET: Home
        [LoginFilter]
        public ActionResult Index()
        {
            return View();
        }

        // GET: Chat
        public ActionResult Chat()
        {
            String name = "name here";
            ViewBag.recipient_name = name;
            return View();
        }
    }
}
