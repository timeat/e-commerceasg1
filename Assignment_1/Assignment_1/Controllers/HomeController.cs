﻿using Assignment_1.ActionFilters;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.Controllers
{
    public class HomeController : Controller
    {
        Models.ManagerEntities5 context = new Models.ManagerEntities5();

        // GET: Home
        [LoginFilter]
        public ActionResult Index(int id)
        {
            int user_id = int.Parse(Session["user_id"].ToString());

            var profiles = from p in context.ProfileInformations select p;
            var profile = context.ProfileInformations.SingleOrDefault(p => p.profile_id == id);

            ViewBag.profile_picture_id = profile.profile_picture;
            ViewBag.profile_id = profile.profile_id;

            return View(profiles.ToList());
        }

        public ActionResult OtherProfile(int id)
        {
            int user_id = int.Parse(Session["user_id"].ToString());

            var profiles = from p in context.ProfileInformations select p;
            var profile = context.ProfileInformations.SingleOrDefault(p => p.profile_id == id);

            ViewBag.profile_picture_id = profile.profile_picture;
            ViewBag.profile_id = profile.profile_id;

            return View(profiles.ToList());
        }

        // GET: Home/Details/5
        public ActionResult Details(int id)
        {
            var thePicture = (from p in context.Pictures
                              where p.picture_id == id
                              select p).FirstOrDefault();

            ViewBag.id = thePicture.profile_id;

            return View(thePicture);
        }

        // GET: Home/Create
        public ActionResult Create(int id, String tag)
        {
            ViewBag.id = id;
            return View();
        }

        // POST: Home/Create
        [HttpPost]
        public ActionResult Create(int id, String tag, FormCollection collection, HttpPostedFileBase picture)
        {
            try
            {
                //get the uploaded picture's image type
                var type = picture.ContentType;
                //make a list of acceptable image types
                string[] acceptableTypes = { "image/jpeg", "image/gif", "image/png" };
                //if the picture exists and is of acceptable image type...
                if (picture != null && picture.ContentLength > 0 && acceptableTypes.Contains(type))
                {
                    //get a string of random letters and numbers
                    Guid g = Guid.NewGuid();
                    //create a file name with the generated string
                    string filename = g + Path.GetExtension(picture.FileName);
                    //create a path to the Pictures folder
                    string path = Path.Combine(Server.MapPath("~/Pictures/"), filename);
                    //save the picture to the Pictures folder with the created path
                    picture.SaveAs(path);


                    //create a new picture object
                    Models.Picture newPic = new Models.Picture()
                    {
                        //populate the picture's fields
                        caption = collection["caption"],
                        relative_path = filename,
                        location = collection["location"],
                        profile_id = id
                    };

                    //validate the new picture
                    TryValidateModel(newPic);
                    //if validation successful...
                    if (ModelState.IsValid)
                    {
                        //add the new picture to the database
                        context.Pictures.Add(newPic);
                        //commit the change to the database
                        context.SaveChanges();







                        //set new profile picture
                        if (tag.Equals("profile"))
                        {
                            Models.ProfileInformation theProfile = (from p in context.ProfileInformations
                                                                    where p.profile_id == id
                                                                    select p).FirstOrDefault();
                            theProfile.profile_picture = newPic.picture_id;

                            context.SaveChanges();




                            /*
                            //remove old profile picture:
                            //get the selected picture by the provided id parameter
                            var thePicture = (from p in context.Pictures
                                              where p.picture_id == theProfile.profile_picture
                                              select p).FirstOrDefault();

                            //remove the picture from the database
                            context.Pictures.Remove(thePicture);
                            //commit the change to the database
                            context.SaveChanges();


                            //get relative path from picture
                            String relativePath = thePicture.relative_path;
                            //get the full path for the picture
                            string fullPath = Request.MapPath("~/Pictures/" + relativePath);
                            //remove it from the Pictures folder if it exists
                            if (System.IO.File.Exists(fullPath))
                            {
                                System.IO.File.Delete(fullPath);
                            }*/
                        }
                        //return to the index page
                        return RedirectToAction("Index", new { id = id });
                    }
                }
                ViewBag.id = id;
                return View();
            }
            catch
            {
                ViewBag.id = id;
                return View();
            }
        }

        // GET: Home/Remove
        public ActionResult Remove(int id)
        {
            //get the respective profile by the provided id parameter
            var theProfile = (from p in context.ProfileInformations
                              where p.profile_id == id
                              select p).FirstOrDefault();

            //get the selected picture by the fethced profile's profile_picture property
            var thePicture = (from p in context.Pictures
                              where p.picture_id == theProfile.profile_picture
                              select p).FirstOrDefault();

            //save the profile id to the ViewBag
            ViewBag.id = thePicture.profile_id;
            //ViewBag.id = id;

            //pass the picture object to the view
            return View(thePicture);
        }

        // POST: Home/Remove
        [HttpPost]
        public ActionResult Remove(int id, FormCollection collection, HttpPostedFileBase picture)
        {
            try
            {
                Models.ProfileInformation theProfile = (from p in context.ProfileInformations
                                                        where p.profile_id == id
                                                        select p).FirstOrDefault();
                theProfile.profile_picture = 0;

                context.SaveChanges();

                return RedirectToAction("Index", new { id = id });
                //return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.id = id;
                return View();
            }
        }

        // GET: Home/Edit/5
        public ActionResult Edit(int id)
        {
            var thePicture = (from p in context.Pictures
                              where p.picture_id == id
                              select p).FirstOrDefault();

            ViewBag.id = thePicture.profile_id;

            return View(thePicture);
        }

        // POST: Home/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                Models.Picture thePicture = (from p in context.Pictures
                                             where p.picture_id == id
                                             select p).FirstOrDefault();
                thePicture.caption = collection["caption"];
                //thePicture.relative_path = collection["relative_path"];
                thePicture.location = collection["location"];

                context.SaveChanges();

                var pid = thePicture.profile_id;

                return RedirectToAction("Index", new { id = pid });
            }
            catch
            {
                return View();
            }
        }

        // GET: Home/Delete/5
        public ActionResult Delete(int id)
        {
            //get the selected picture by the provided id parameter
            var thePicture = (from p in context.Pictures
                              where p.picture_id == id
                              select p).FirstOrDefault();

            //save the profile id to the ViewBag
            ViewBag.id = thePicture.profile_id;

            //pass the picture object to the view
            return View(thePicture);
        }

        // POST: Home/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection, HttpPostedFileBase picture)
        {
            try
            {
                //get the selected picture by the provided id parameter
                var thePicture = (from p in context.Pictures
                                  where p.picture_id == id
                                  select p).FirstOrDefault();

                //get the profile id from the picture
                var pid = thePicture.profile_id;


                //remove the picture from the database
                context.Pictures.Remove(thePicture);
                //commit the change to the database
                context.SaveChanges();


                //get relative path from picture
                String relativePath = thePicture.relative_path;
                //get the full path for the picture
                string fullPath = Request.MapPath("~/Pictures/" + relativePath);
                //remove it from the Pictures folder if it exists
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }


                //return to the index page
                return RedirectToAction("Index", new { id = pid });
            }
            catch
            {
                return View();
            }
        }
        // GET: Home/Create
        public ActionResult SendMessage(int id, String tag)
        {
            ViewBag.id = id;
            return View();
        }

        // POST: Home/Create
        [HttpPost]
        public ActionResult SendMessage(int id, FormCollection collection)
        {
            try
            {
                int idSender = int.Parse(Session["user_id"].ToString());
                Models.Message newMessage = new Models.Message()
                {
                    sender = idSender,
                    receiver = id,
                    message1 = collection["message1"]
                };

                context.Messages.Add(newMessage);
                context.SaveChanges();
                return RedirectToAction("Index", new { id = idSender});
                
            }
            catch
            {
                
                ViewBag.idRec = id;
                return View();
            }
        }
        public ActionResult SendFriendRequest(int id)
        {
            
            {
                int idSender = int.Parse(Session["user_id"].ToString());
                Models.FriendLink newFriend = new Models.FriendLink()
                {
                    requester = idSender,
                    requested = id,
                    type = 0,
                    timestamp = DateTime.Now
                };

                context.FriendLinks.Add(newFriend);
                context.SaveChanges();
                return RedirectToAction("Index", new { id = id });
            }
           
        }

        [HttpPost]
        public ActionResult CommentCreate(int id, String comment1)
        {

            // TODO: Add insert logic here
            int user_id = int.Parse(Session["user_id"].ToString());
            Models.Comment newComment = new Models.Comment()
            {
                picture_id = id,
                profile_id = user_id,
                comment1 = comment1
            };
            context.Comments.Add(newComment);
            context.SaveChanges();
            return RedirectToAction("OtherProfile", new { id = id });

        }


        // POST: Like/Create
        [HttpPost]
        public ActionResult LikeCreate(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                int user_id = int.Parse(Session["user_id"].ToString());
                Models.Like newLike = new Models.Like()
                {
                    picture_id = id,
                    profile_id = user_id,
                    type = 1
                };
                context.Likes.Add(newLike);
                context.SaveChanges();
                return RedirectToAction("Index", new { id = id });

            }
            catch
            {
                return View();
            }
        }
    }
}
