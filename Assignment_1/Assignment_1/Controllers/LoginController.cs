﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Assignment_1.Controllers
{
    public class LoginController : Controller
    {
        Models.ManagerEntities5 context = new Models.ManagerEntities5();
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        // POST: Login/Create
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            try
            {
                string username = collection["user_name"];

                var theUser = context.Users.SingleOrDefault(u => u.user_name == username);

                if (theUser != null)
                {
                    Session["user_id"] = theUser.user_id;
                    Session["username"] = theUser.user_name;

                    var theProfile = context.ProfileInformations.FirstOrDefault(p => p.user_name == username);
                    if (theProfile == null)
                    {
                        return RedirectToAction("Create", "Profile", new { id = theUser.user_id, user_name = theUser.user_name });
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home", new { id = theUser.user_id });
                    }
                }
                else
                {
                    return Content("The username does not exist. Click \"Register\" to create a user.");
                }
            }
            catch
            {
                return Content("Login error!");
            }
        }

        // GET: Login/Register
        public ActionResult Register()
        {
            return View();
        }

        // POST: Login/Register
        [HttpPost]
        public ActionResult Register(FormCollection collection)
        {
            //try
            {
                // TODO: Add insert logic here
                Models.User newUser = new Models.User()
                {
                    user_name = collection["user_name"],
                    password = Crypto.HashPassword(collection["password"])
                };
                context.Users.Add(newUser);
                context.SaveChanges();

                return RedirectToAction("Create", "Profile", new { id = newUser.user_id, user_name = newUser.user_name });
            }
            //catch
            {
                //return View();
            }
        }

        // GET: Login/
        public ActionResult Logout()
        {
            Session.Abandon();

            return RedirectToAction("Index");
        }
    }
}



//use in query to reset identity count:
//DBCC CHECKIDENT ([User], RESEED, 0)