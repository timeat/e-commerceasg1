﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.Controllers
{
    public class MessageController : Controller
    {
        Models.ManagerEntities5 context = new Models.ManagerEntities5();

        // GET: Message
        public ActionResult Index(int id)
        {
            var theProfile = context.ProfileInformations.Single(x => x.profile_id == id);
            ViewBag.id = id;
            return View(theProfile);
        }

        // GET: Message/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Message/Create
        public ActionResult Create(int id)
        {
            ViewBag.id = id;
            return View();
        }

        // POST: Message/Create
        [HttpPost]
        public ActionResult Create(int id, FormCollection collection)
        {

            //try
            //{
            // TODO: Add insert logic here
                String username = collection["receiver"];
                Models.ProfileInformation profile = context.ProfileInformations.Single(x => x.user_name == username);
                Models.Message newMessage = new Models.Message()
                {
                    sender = id,
                    receiver = profile.profile_id,
                    message1 = collection["message1"]
                };

                context.Messages.Add(newMessage);
                context.SaveChanges();
                return RedirectToAction("Index", new { id = id });
            //}
            //catch
            //{
            //    return View();
            //}
        }

        // GET: Message/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Message/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Message/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Message/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
