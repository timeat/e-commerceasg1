﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.Controllers
{
    public class PictureController : Controller
    {
        Models.ManagerEntities5 context = new Models.ManagerEntities5();

        // GET: Picture
        public ActionResult Index(int id)
        {

            var profile = (from p in context.ProfileInformations
                           where p.profile_id == id
                           select p).FirstOrDefault();

            return View(profile);
        }

        // GET: Picture/Details/5
        public ActionResult Details(int id)
        {
            var thePicture = (from p in context.Pictures
                              where p.picture_id == id
                              select p).FirstOrDefault();

            ViewBag.id = thePicture.profile_id;

            return View(thePicture);
        }

        // GET: Picture/Create
        public ActionResult Create(int id)
        {
            ViewBag.id = id;
            return View();
        }

        // POST: Picture/Create
        [HttpPost]
        public ActionResult Create(int id, FormCollection collection, HttpPostedFileBase picture)
        {
            try
            {
                //get the uploaded picture's image type
                var type = picture.ContentType;
                //make a list of acceptable image types
                string[] acceptableTypes = { "image/jpeg", "image/gif", "image/png" };
                //if the picture exists and is of acceptable image type...
                if (picture != null && picture.ContentLength > 0 && acceptableTypes.Contains(type))
                {
                    //get a string of random letters and numbers
                    Guid g = Guid.NewGuid();
                    //create a file name with the generated string
                    string filename = g + Path.GetExtension(picture.FileName);
                    //create a path to the Pictures folder
                    string path = Path.Combine(Server.MapPath("~/Pictures/"), filename);
                    //save the picture to the Pictures folder with the created path
                    picture.SaveAs(path);


                    //create a new picture object
                    Models.Picture newPic = new Models.Picture()
                    {
                        //populate the picture's fields
                        caption = collection["caption"],
                        relative_path = filename,
                        location = collection["location"],
                        profile_id = id
                    };

                    //validate the new picture
                    TryValidateModel(newPic);
                    //if validation successful...
                    if (ModelState.IsValid)
                    {
                        //add the new picture to the database
                        context.Pictures.Add(newPic);
                        //commit the change to the database
                        context.SaveChanges();


                        //return to the index page
                        return RedirectToAction("Index", new { id = id });
                    }
                }
                ViewBag.id = id;
                return View();
            }
            catch
            {
                ViewBag.id = id;
                return View();
            }
        }

        // GET: Picture/Edit/5
        public ActionResult Edit(int id)
        {
            var thePicture = (from p in context.Pictures
                              where p.picture_id == id
                              select p).FirstOrDefault();

            ViewBag.id = thePicture.profile_id;

            return View(thePicture);
        }

        // POST: Picture/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                Models.Picture thePicture = (from p in context.Pictures
                                             where p.picture_id == id
                                             select p).FirstOrDefault();
                thePicture.caption = collection["caption"];
                //thePicture.relative_path = collection["relative_path"];
                thePicture.location = collection["location"];

                context.SaveChanges();

                var pid = thePicture.profile_id;

                return RedirectToAction("Index", new { id = pid });
            }
            catch
            {
                return View();
            }
        }

        // GET: Picture/Delete/5
        public ActionResult Delete(int id)
        {
            //get the selected picture by the provided id parameter
            var thePicture = (from p in context.Pictures
                              where p.picture_id == id
                              select p).FirstOrDefault();

            //save the profile id to the ViewBag
            ViewBag.id = thePicture.profile_id;

            //pass the picture object to the view
            return View(thePicture);
        }

        // POST: Picture/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection, HttpPostedFileBase picture)
        {
            try
            {
                //get the selected picture by the provided id parameter
                var thePicture = (from p in context.Pictures
                                  where p.picture_id == id
                                  select p).FirstOrDefault();

                //get the profile id from the picture
                var pid = thePicture.profile_id;


                //remove the picture from the database
                context.Pictures.Remove(thePicture);
                //commit the change to the database
                context.SaveChanges();


                //get relative path from picture
                String relativePath = thePicture.relative_path;
                //get the full path for the picture
                string fullPath = Request.MapPath("~/Pictures/" + relativePath);
                //remove it from the Pictures folder if it exists
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }


                //return to the index page
                return RedirectToAction("Index", new { id = pid });
            }
            catch
            {
                return View();
            }
        }
    }
}
