﻿using Assignment_1.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Assignment_1.Controllers
{
    public class ProfileController : Controller
    {
        Models.ManagerEntities5 context = new Models.ManagerEntities5();

        // GET: Profile
        //public async Task<ActionResult> Index(string searchString)
        //[LoginFilter]
        public ActionResult Index(int id)
        {
            var profile = context.ProfileInformations.SingleOrDefault(p => p.profile_id == id);
            
            return View(profile);
        }

        // GET: Profile/Create
        public ActionResult Create(int id, String user_name)
        {
            ViewBag.id = id;
            ViewBag.genders = new SelectList(context.Genders, "gender_id", "gender_name");
            return View();
        }

        // POST: Profile/Create
        [HttpPost]
        public ActionResult Create(int id, String user_name, FormCollection collection)
        {
           // try
            {
                Models.ProfileInformation newProfile = new Models.ProfileInformation()
                {
                    profile_id = id,
                    name = collection["name"],
                    user_name = user_name,
                    website = collection["website"],
                    biography = collection["biography"],
                    email_address = collection["email_address"],
                    phone_number = collection["phone_number"],
                    profile_picture = null,
                    gender_id = int.Parse(collection["gender_id"]),
                    user_id = id,
                };

                context.ProfileInformations.Add(newProfile);
                context.SaveChanges();

                return RedirectToAction("Index", "Home", new { id = id });
            }
           // catch
            {
           //     ViewBag.id = id;
           //     return View();
            }
        }

        // GET: Profile/Edit/5
        public ActionResult Edit(int id)
        {
            var theProfile = (from p in context.ProfileInformations
                              where p.profile_id == id
                              select p).FirstOrDefault();

            ViewBag.id = theProfile.profile_id;
            ViewBag.genders = new SelectList(context.Genders, "gender_id", "gender_name");

            return View(theProfile);
        }

        // POST: Profile/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            //try
            {
                Models.ProfileInformation theProfile = (from p in context.ProfileInformations
                                             where p.profile_id == id
                                             select p).FirstOrDefault();
                theProfile.name = collection["name"];
                theProfile.website = collection["website"];
                theProfile.biography = collection["biography"];
                theProfile.email_address = collection["email_address"];
                theProfile.phone_number = collection["phone_number"];
                //theProfile.gender_id = int.Parse(collection["gender_id"]);
                theProfile.user_name = collection["user_name"];

                Models.User theUser = (from u in context.Users
                                                        where u.user_id == id
                                                        select u).FirstOrDefault();
                theUser.user_name = collection["user_name"];
                theUser.password = Crypto.HashPassword(collection["User.password"]);

                context.SaveChanges();

                var pid = theProfile.profile_id;

                try
                {
                    string username = collection["user_name"];

                    if (theUser != null)
                    {
                        Session["user_id"] = theUser.user_id;
                        Session["username"] = theUser.user_name;

                        return RedirectToAction("Index", "Home", new { id = id });
                    }
                    return RedirectToAction("Index", "Home", new { id = id });
                }
                catch
                {
                    return Content("Login error!");
                }
            }
            //catch
            {
                //return View();
            }
        }
    }
}
